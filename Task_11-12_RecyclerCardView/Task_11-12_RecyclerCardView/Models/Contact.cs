﻿namespace Task_11_12_RecyclerCardView.Models
{
	public class Contact
	{
		public long Id { get; set; }

		public string FirstName { get; set; }

		public string Patronymic { get; set; }

		public string LastName { get; set; }

		public string PhoneNumber { get; set; }

		public byte[] Avatar { get; set; }

		public string Address { get; set; }
	}
}