﻿using System.Collections.Generic;
using System.Linq;

namespace Task_11_12_RecyclerCardView.Models
{
	public static class ContactsDb
	{
		private static List<Contact> Contacts { get; }
		private static long LastId = 0;

		static ContactsDb()
		{
			Contacts = new List<Contact>();
			InitList();
		}

		public static void InitList()
		{
			AddContact(new Contact { FirstName = "Eugene", Patronymic = "Nicolaevich", LastName = "Timchenko", PhoneNumber = "(097)555-44-33", Address = "Mira 1, kv. 1" });
			AddContact(new Contact { FirstName = "Kostya", Patronymic = "Nicolaevich", LastName = "Gerasimenko", PhoneNumber = "(097)555-44-33", Address = "Mira 2, kv. 2" });
			AddContact(new Contact { FirstName = "Denis", Patronymic = "Sergeevich", LastName = "Kohan", PhoneNumber = "(097)555-44-33", Address = "Mira 3, kv. 3" });
			AddContact(new Contact { FirstName = "Alexander", Patronymic = "Evgenievich", LastName = "Popel", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
			AddContact(new Contact { FirstName = "Maria", Patronymic = "Kovarova", LastName = "Kovarova", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
			AddContact(new Contact { FirstName = "Anton", Patronymic = "Vasko", LastName = "Vasko", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
			AddContact(new Contact { FirstName = "Vasya", Patronymic = "Vesyolov", LastName = "Vesyolov", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
			AddContact(new Contact { FirstName = "Pasha", Patronymic = "Petrov", LastName = "Petrov", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
			AddContact(new Contact { FirstName = "Petya", Patronymic = "Neznaykin", LastName = "Neznaykin", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
			AddContact(new Contact { FirstName = "Sergey", Patronymic = "Zamukov", LastName = "Zamukov", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
			AddContact(new Contact { FirstName = "Kolya", Patronymic = "Verdov", LastName = "Verdov", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
			AddContact(new Contact { FirstName = "Miroslav", Patronymic = "Petrushin", LastName = "Petrushin", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
			AddContact(new Contact { FirstName = "Slava", Patronymic = "Milleon", LastName = "Milleon", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
			AddContact(new Contact { FirstName = "Vitaliy", Patronymic = "JooBoo", LastName = "JooBoo", PhoneNumber = "(097)555-44-33", Address = "Mira 4, kv. 4" });
		}

		public static void AddContact(Contact contact)
		{
			contact.Id = ++LastId;
			Contacts.Add(contact);
		} 

		public static Contact GetById(long id)
		{
			return Contacts.FirstOrDefault(c => c.Id == id);
		}

		public static bool DeleteById(long id)
		{
			var contact = Contacts.FirstOrDefault(c => c.Id == id);
			return Contacts.Remove(contact);
		}

        public static Contact GetByPosition(int position)
        {
            return Contacts[position];
        }

        public static void DeleteByPosition(int position)
        {
            Contacts.RemoveAt(position);
        }

        public static int Count()
		{
			return Contacts.Count;
		}
	}
}