﻿using System;
using Android;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Content;
using Task_11_12_RecyclerCardView.Models;
using System.Threading.Tasks;
using Android.Views.InputMethods;

namespace Task_11_12_RecyclerCardView
{
	[Activity(Label = "Task_11-12", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		private RecyclerView _recyclerView;
		private RecyclerView.LayoutManager _layoutManager;
		private ContactAdapter _adapter;
		private Button _addContactBtn;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.Main);

			_addContactBtn = FindViewById<Button>(Resource.Id.add_new);
			_addContactBtn.Click += OnAddContact;

			_recyclerView = FindViewById<RecyclerView>(Resource.Id.contacts_list);
			_layoutManager = new LinearLayoutManager(this);
			_adapter = new ContactAdapter();
			_adapter.ItemClick += OnItemClick;
			_adapter.DeleteClick += OnDeleteClick;

			_recyclerView.SetLayoutManager(_layoutManager);
			_recyclerView.SetAdapter(_adapter);
		}

		void OnItemClick(object sender, long id)
		{
			ShowContact(id);
		}

		void OnDeleteClick(object sender, long id)
		{
			DeleteContact(id);
		}

		private void ShowContact(long id)
		{
			var info = new Intent(this, typeof(ContactInfoActivity));
			info.PutExtra("contact_id", id);
			StartActivity(info);
		}

		private void OnAddContact(object sender, EventArgs e)
		{
			AddNewContactFragment fragment = new AddNewContactFragment();
			fragment.Show(FragmentManager, "add_contact");
		}

		public void OnAddContactFragmentResult(int result)
		{
			if(result == AddNewContactFragment.CONTACT_ADDED)
			{
				Toast.MakeText(this, "The new contact is added", ToastLength.Short).Show();
				_adapter.NotifyDataSetChanged();
			}
				
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();

			if(_addContactBtn != null)
			{
				_addContactBtn.Click -= OnAddContact;
				_addContactBtn.Dispose();
				_addContactBtn = null;
			}
			
			_recyclerView?.Dispose();
			_recyclerView = null;
		}

		private void DeleteContact(long id)
		{
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.SetMessage("Are you sure you want to delete the contact?");
			dialog.SetPositiveButton(Resource.String.ok_btn, (d, p) =>
			{
				ContactsDb.DeleteById(id);
				Toast.MakeText(this, "The contact is deleted", ToastLength.Short).Show();
				_adapter.NotifyDataSetChanged();

			});

			dialog.SetNegativeButton(Resource.String.cancel_btn, (d, p) => { });

			dialog.Show();

		}
	}


	public class ContactAdapter : RecyclerView.Adapter
	{
		public event EventHandler<long> ItemClick;
		public event EventHandler<long> DeleteClick;

		public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
		{
			ContactViewHolder viewHolder = holder as ContactViewHolder;
			viewHolder.Name.Text = ContactsDb.GetByPosition(position).FirstName + " " + ContactsDb.GetByPosition(position).LastName;
			viewHolder.PhoneNumber.Text = ContactsDb.GetByPosition(position).PhoneNumber;
			viewHolder.ContactId = ContactsDb.GetByPosition(position).Id;
		}

		public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
		{
			View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.ContactCard, parent, false);

			ContactViewHolder viewHolder = new ContactViewHolder(itemView, OnItemClick, OnDeleteButtonClick);
			return viewHolder;
		}

		public override int ItemCount => ContactsDb.Count();

		void OnItemClick(long id)
		{
			ItemClick(this, id);
		}

		void OnDeleteButtonClick(long id)
		{
			DeleteClick(this, id);
		}
	}

	public class ContactViewHolder : RecyclerView.ViewHolder
	{
		public TextView Name;
		public TextView PhoneNumber;
		public ImageButton DeleteContact;
		public long ContactId;

		public ContactViewHolder(View itemView, Action<long> itemListener, Action<long> deleteListener) : base(itemView)
		{
			Name = itemView.FindViewById<TextView>(Resource.Id.name);
			PhoneNumber = itemView.FindViewById<TextView>(Resource.Id.phone_number);
			DeleteContact = itemView.FindViewById<ImageButton>(Resource.Id.delete_btn);

			itemView.Click += (sender, e) => itemListener(ContactId);
			DeleteContact.Click += (sender, e) => deleteListener(ContactId);
		}
	}
}