﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using Task_11_12_RecyclerCardView.Models;

namespace Task_11_12_RecyclerCardView
{
	[Activity(Label = "Information")]
	public class ContactInfoActivity : Activity
	{
		private TextView _firstNameField;
		private TextView _patronymicField;
		private TextView _lastNameField;
		private TextView _phoneNumberField;
		private TextView _addressField;
		private TextView _fullNameField;

		private Contact _contact;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			
			SetContentView(Resource.Layout.contact_info);

			_firstNameField = FindViewById<TextView>(Resource.Id.first_name);
			_patronymicField = FindViewById<TextView>(Resource.Id.patronymic);
			_lastNameField = FindViewById<TextView>(Resource.Id.last_name);
			_phoneNumberField = FindViewById<TextView>(Resource.Id.phone_number);
			_addressField = FindViewById<TextView>(Resource.Id.address);
			_fullNameField = FindViewById<TextView>(Resource.Id.full_name);

			_contact = ContactsDb.GetById(Intent.GetLongExtra("contact_id", -1));

			if (_contact == null)
				throw new ArgumentNullException();
		}

		protected override void OnStart()
		{
			base.OnStart();

			_firstNameField.Text = _contact.FirstName;
			_patronymicField.Text = _contact.Patronymic;
			_lastNameField.Text = _contact.LastName;
			_phoneNumberField.Text = _contact.PhoneNumber;
			_addressField.Text = _contact.Address;
			_fullNameField.Text = _contact.FirstName + " " + _contact.LastName;
		}
	}
}