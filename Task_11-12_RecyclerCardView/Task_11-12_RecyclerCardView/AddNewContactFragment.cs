﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Telephony;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Task_11_12_RecyclerCardView.Models;
using static Android.Widget.TextView;

namespace Task_11_12_RecyclerCardView
{
	public class AddNewContactFragment : DialogFragment
	{
		private EditText _firstNameInput;
		private EditText _patronymicInput;
		private EditText _lastNameInput;
		private EditText _phoneNumberInput;
		private EditText _addressInput;

		private Button _addBtn;
		private Button _cancelBtn;

		public const int CONTACT_ADDED = 1;
		public const int ADDING_CANCELED = 2;

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			//Dialog.Window.SetLayout(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
			SetStyle(DialogFragmentStyle.Normal, Resource.Style.FullScreenDialog);
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate(Resource.Layout.AddNewContact, container);

			_firstNameInput = view.FindViewById<EditText>(Resource.Id.add_first_name);
			_patronymicInput = view.FindViewById<EditText>(Resource.Id.add_patronymic);
			_lastNameInput = view.FindViewById<EditText>(Resource.Id.add_last_name);
			_phoneNumberInput = view.FindViewById<EditText>(Resource.Id.add_phone_number);
			_addressInput = view.FindViewById<EditText>(Resource.Id.add_address);

			_addBtn = view.FindViewById<Button>(Resource.Id.contact_add_btn);
			_cancelBtn = view.FindViewById<Button>(Resource.Id.contact_cancel_btn);

			_addressInput.EditorAction += (s, e) => {
				if (e.ActionId == ImeAction.Done)
				{
					OnAddContact(s, e);
				}
			};

			_addBtn.Click += OnAddContact;
			_cancelBtn.Click += (s, e) => { Dismiss(); };

			_phoneNumberInput.AddTextChangedListener(new PhoneNumberFormattingTextWatcher());

			return view;
		}


		public override void OnResume()
		{
			base.OnResume();
			
			InputMethodManager imm = (InputMethodManager) Activity.GetSystemService(Context.InputMethodService);
			imm.ShowSoftInput(_firstNameInput, ShowFlags.Forced);
		}

		private void OnAddContact(object sender, EventArgs e)
		{
			var result = AddContact();

			if(result)
			{
				var parent = Activity as MainActivity;
				parent.OnAddContactFragmentResult(CONTACT_ADDED);
				Dismiss();
			}
		}

		private bool AddContact()
		{
			var firstName = _firstNameInput.Text;
			var patronymic = _patronymicInput.Text;
			var lastName = _lastNameInput.Text;
			var phoneNumber = _phoneNumberInput.Text;
			var address = _addressInput.Text;
			if (string.IsNullOrWhiteSpace(firstName) ||
				string.IsNullOrWhiteSpace(patronymic) ||
				string.IsNullOrWhiteSpace(lastName) ||
				string.IsNullOrWhiteSpace(phoneNumber) ||
				string.IsNullOrWhiteSpace(address))
			{
				return false;
			}
				
			else
			{
				ContactsDb.AddContact(new Contact { FirstName = firstName, Patronymic = patronymic, LastName = lastName, PhoneNumber = phoneNumber, Address = address });
			}
			
			return true;
		}
	}
}