﻿using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using com.altexsoft.timchenko.todolist.Helpers;
using com.altexsoft.timchenko.todolist.Models;
using com.altexsoft.timchenko.todolist.ViewModels;

namespace com.altexsoft.timchenko.todolist.Views
{
	public class TodoListAdapter : RecyclerView.Adapter
	{
		private readonly Dictionary<TaskDayGroup, List<TodoTask>> _todoUnits;
		private Context _ctx;

		public TodoListAdapter(Context ctx, Dictionary<TaskDayGroup, List<TodoTask>> todoUnits)
		{
			_todoUnits = todoUnits;
			_ctx = ctx;
		}

		public override int ItemCount => _todoUnits.TotalCount();

		public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
		{
			TodoTaskViewModel viewModel = _todoUnits.GetByPositionOrDefalut(position);

			var viewHolder = holder as TodoListHolder;

			viewHolder.TaskTitle.Text = viewModel.TodoTask.Title;
			viewHolder.TaskDescription.Text = viewModel.TodoTask.Descrition;
			viewHolder.TaskGroupDay.Text = viewModel.TodoTask.Descrition;
		}

		public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
		{
			View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.todo_task, parent, false);

			var viewHolder = new TodoListHolder(itemView);
			return viewHolder;
		}
	}

	public class TodoListHolder : RecyclerView.ViewHolder
	{
		public TextView TaskGroupDate;
		public TextView TaskGroupDay;
		public TextView TaskTitle;
		public TextView TaskDescription;
		public TextView TaskCategory;

		public TodoListHolder(View itemView) : base(itemView)
		{
			TaskGroupDate = itemView.FindViewById<TextView>(Resource.Id.date_group);
			TaskGroupDay = itemView.FindViewById<TextView>(Resource.Id.date_group_day);
			TaskTitle = itemView.FindViewById<TextView>(Resource.Id.task_title);
			TaskDescription = itemView.FindViewById<TextView>(Resource.Id.task_category);
		}
	}
}