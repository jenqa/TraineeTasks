﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using com.altexsoft.timchenko.todolist.Models;
using com.altexsoft.timchenko.todolist.Services;

namespace com.altexsoft.timchenko.todolist.Views
{
	[Activity(Label = "ToDo_List_Final", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : AppCompatActivity
	{
		private TodoListAdapter _adapter;

		private FloatingActionButton _addTodoUnitBtn;
		private RecyclerView.LayoutManager _layoutManager;

		private RecyclerView _todoList;
		private ITodoService _todoService;
		private Dictionary<TaskDayGroup, List<TodoTask>> _displayedTasks;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.activity_main);

			_todoService = new TodoMemoryService();
			InitDisplayedTasks();

			_todoList = FindViewById<RecyclerView>(Resource.Id.todo_list);
			_layoutManager = new LinearLayoutManager(this);
			_adapter = new TodoListAdapter(this, _displayedTasks);
			_todoList.SetLayoutManager(_layoutManager);
			_todoList.SetAdapter(_adapter);

			_addTodoUnitBtn = FindViewById<FloatingActionButton>(Resource.Id.add_todo_unit);

			_addTodoUnitBtn.Click += OnAddUnitClick;
		}

		private void OnAddUnitClick(object sender, EventArgs e)
		{
		}

		private void InitDisplayedTasks()
		{
			_displayedTasks = new Dictionary<TaskDayGroup, List<TodoTask>>();
			
			_displayedTasks.Add(TaskDayGroup.GetDayGroup(TaskDayGroup.GroupType.Today), _todoService.GetAllForToday());
		}
	}
}