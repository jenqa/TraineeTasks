﻿using Android.Graphics;

namespace com.altexsoft.timchenko.todolist.Models
{
	public class TaskCategory
	{
		public string Title { get; set; }

		public Color Color { get; set; }
	}
}