﻿using System;

namespace com.altexsoft.timchenko.todolist.Models
{
	public class TodoTask : IComparable<TodoTask>
	{
		public long Id { get; set; }

		public string Title { get; set; }

		public string Descrition { get; set; }

		public DateTime StartTime { get; set; }

		public bool Done { get; set; }

		public TaskCategory Category { get; set; }

		// Interface methods
		public int CompareTo(TodoTask other)
		{
			return StartTime.CompareTo(other.StartTime);
		}
	}
}