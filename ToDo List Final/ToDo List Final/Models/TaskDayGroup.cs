﻿using Android.Graphics;
using com.altexsoft.timchenko.todolist.Helpers;

namespace com.altexsoft.timchenko.todolist.Models
{
	public class TaskDayGroup
	{
		public enum GroupType : byte
		{
			Past,
			Today,
			Tomorrow,
			Next7Days,
			NotSoon
		}

		public string Title { get; }

		public Color Color { get; }

		private TaskDayGroup(string title, Color color)
		{
			Title = title;
			Color = color;
		}

		public static TaskDayGroup GetDayGroup(GroupType type)
		{
			switch (type)
			{
				case GroupType.NotSoon:
					return new TaskDayGroup("Not soon", ColorsRetriever.NextColor());

				case GroupType.Next7Days:
					return new TaskDayGroup("Next 7 days", ColorsRetriever.NextColor());

				case GroupType.Tomorrow:
					return new TaskDayGroup("Tomorrow", ColorsRetriever.NextColor());

				case GroupType.Today:
					return new TaskDayGroup("Today", ColorsRetriever.NextColor());

				case GroupType.Past:
					return new TaskDayGroup("Past", ColorsRetriever.NextColor());

				default:
					return new TaskDayGroup("Today", ColorsRetriever.NextColor());
			}
		}
	}
}