﻿using System;
using System.Collections.Generic;
using com.altexsoft.timchenko.todolist.Models;

namespace com.altexsoft.timchenko.todolist.Dal
{
	public static class TodoDb
	{
		private static long _lastId;

		private static readonly List<TodoTask> TodoUnits;

		static TodoDb()
		{
			TodoUnits = new List<TodoTask>();
			Insert(new TodoTask {Title = "Lorem ipsum dolor sit amet", StartTime = new DateTime(2017, 5, 20), Category = new TaskCategory() { Title = "Family"} });
			Insert(new TodoTask {Title = "Lorem ipsum dolor sit amet", StartTime = new DateTime(2017, 5, 20), Category = new TaskCategory() { Title = "Family"} });
			Insert(new TodoTask {Title = "Lorem ipsum dolor sit amet", StartTime = new DateTime(2017, 5, 20), Category = new TaskCategory() { Title = "Family"} });
			Insert(new TodoTask {Title = "Lorem ipsum dolor sit amet", StartTime = new DateTime(2017, 5, 20), Category = new TaskCategory() { Title = "Family"} });
			Insert(new TodoTask {Title = "Lorem ipsum dolor sit amet", StartTime = new DateTime(2017, 5, 20), Category = new TaskCategory() { Title = "Family"} });
			//Insert(new TodoTask {Title = "Suspendisse pharetra felis eget dictum tempus", StartTime = new DateTime(2017, 5, 18)});
			//Insert(new TodoTask
			//{
			//	Title = "Nulla suscipit tellus vitae ipsum blandit sollicitudin",
			//	StartTime = new DateTime(2017, 5, 17)
			//});
			//Insert(new TodoTask {Title = "Donec blandit pharetra purus non blandit", StartTime = new DateTime(2017, 5, 18)});
			//Insert(new TodoTask {Title = "Nunc sollicitudin rhoncus ultricies", StartTime = new DateTime(2017, 5, 17)});
			//Insert(new TodoTask {Title = "Etiam diam nunc, interdum accumsan lacus eget", StartTime = new DateTime(2017, 5, 19)});
			//Insert(new TodoTask
			//{
			//	Title = "Praesent consectetur dictum justo vel efficitur",
			//	StartTime = new DateTime(2017, 5, 20)
			//});
			//Insert(new TodoTask {Title = "Ut in nisl molestie", StartTime = new DateTime(2017, 5, 16)});
			//Insert(new TodoTask {Title = "Vivamus dapibus iaculis sagittis", StartTime = new DateTime(2017, 5, 16)});
			//Insert(new TodoTask {Title = "Donec bibendum", StartTime = new DateTime(2017, 5, 18)});
			//Insert(new TodoTask
			//{
			//	Title = "Aenean varius purus vel elit pulvinar efficitur",
			//	StartTime = new DateTime(2017, 5, 17)
			//});
			//Insert(new TodoTask
			//{
			//	Title = "In sit amet diam eu nibh ultricies mattis ac sed nisi",
			//	StartTime = new DateTime(2017, 5, 20)
			//});
			//Insert(new TodoTask {Title = "Quisque scelerisque", StartTime = new DateTime(2017, 6, 21)});
			//Insert(new TodoTask {Title = "Cras sit amet erat rutrum", StartTime = new DateTime(2017, 6, 22)});
		}

		public static int Count => TodoUnits.Count;

		public static List<TodoTask> GetAll()
		{
			return TodoUnits;
		}

		public static TodoTask GetByPosition(int position)
		{
			return TodoUnits[position];
		}

		public static void Insert(TodoTask todoUnit)
		{
			todoUnit.Id = ++_lastId;
			todoUnit.StartTime = todoUnit.StartTime.Date;

			TodoUnits.Add(todoUnit);
		}
	}
}