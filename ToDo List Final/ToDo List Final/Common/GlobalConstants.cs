﻿namespace com.altexsoft.timchenko.todolist.Common
{
	public static class GlobalConstants
	{
		public static class Colors
		{
			public static string LINE_COLOR_1 = "#EF5350";
			public static string LINE_COLOR_2 = "#7E57C2";
			public static string LINE_COLOR_3 = "#4CAF50";
			public static string LINE_COLOR_4 = "#E91E63";
			public static string LINE_COLOR_5 = "#03A9F4";
			public static string LINE_COLOR_6 = "#8E24AA";
			public static string LINE_COLOR_7 = "#009688";
			public static string LINE_COLOR_8 = "#F57C00";
			public static string LINE_COLOR_9 = "#1DE9B6";
		}
	}
}