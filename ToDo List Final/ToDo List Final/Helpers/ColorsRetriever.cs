﻿using Android.Graphics;
using com.altexsoft.timchenko.todolist.Common;

namespace com.altexsoft.timchenko.todolist.Helpers
{
	public static class ColorsRetriever
	{
		private static int currentColor;

		public static Color NextColor()
		{
			return ChooseColor(true);
		}

		public static Color CurrentColor()
		{
			return ChooseColor();
		}

		private static Color ChooseColor(bool needNextColor = false)
		{
			if (needNextColor)
				currentColor++;

			if (currentColor > 9)
				currentColor = 1;

			string strColor;

			switch (currentColor)
			{
				case 1:
					strColor = GlobalConstants.Colors.LINE_COLOR_1;
					break;

				case 2:
					strColor = GlobalConstants.Colors.LINE_COLOR_2;
					break;

				case 3:
					strColor = GlobalConstants.Colors.LINE_COLOR_3;
					break;

				case 4:
					strColor = GlobalConstants.Colors.LINE_COLOR_4;
					break;

				case 5:
					strColor = GlobalConstants.Colors.LINE_COLOR_5;
					break;

				case 6:
					strColor = GlobalConstants.Colors.LINE_COLOR_6;
					break;

				case 7:
					strColor = GlobalConstants.Colors.LINE_COLOR_7;
					break;

				case 8:
					strColor = GlobalConstants.Colors.LINE_COLOR_8;
					break;

				case 9:
					strColor = GlobalConstants.Colors.LINE_COLOR_9;
					break;

				default:
					strColor = GlobalConstants.Colors.LINE_COLOR_1;
					break;
			}

			return Color.ParseColor(strColor);
		}
	}
}