﻿using System.Collections.Generic;
using com.altexsoft.timchenko.todolist.Models;
using com.altexsoft.timchenko.todolist.ViewModels;

namespace com.altexsoft.timchenko.todolist.Helpers
{
	public static class DictionaryExtensions
	{
		public static TodoTaskViewModel GetByPositionOrDefalut(this Dictionary<TaskDayGroup, List<TodoTask>> dictionary, int position)
		{
			var currentPosition = 0;
			TodoTaskViewModel result = null;

			// TODO: ADD INDEX AND IS FIRST FOR DAY

			foreach (var kvPair in dictionary)
				foreach (var todoTask in kvPair.Value)
				{
					if (currentPosition == position)
					{
						result = new TodoTaskViewModel();
						result.TaskDayGroup = kvPair.Key;
						result.TodoTask = todoTask;

						return result;
					}

					currentPosition++;
				}

			return result;
		}

		public static int TotalCount(this Dictionary<TaskDayGroup, List<TodoTask>> dictionary)
		{
			var result = 0;

			foreach (var i in dictionary)
				result += i.Value.Count;

			return result;
		}
	}
}