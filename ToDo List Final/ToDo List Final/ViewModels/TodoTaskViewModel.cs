﻿
using com.altexsoft.timchenko.todolist.Models;

namespace com.altexsoft.timchenko.todolist.ViewModels
{
	public class TodoTaskViewModel
	{
		public TaskDayGroup TaskDayGroup { get; set; }

		public TodoTask TodoTask { get; set; }

		public bool IsFirstForDay { get; set; }
	}
}