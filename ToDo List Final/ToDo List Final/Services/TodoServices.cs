﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.altexsoft.timchenko.todolist.Dal;
using com.altexsoft.timchenko.todolist.Models;

namespace com.altexsoft.timchenko.todolist.Services
{
	public interface ITodoService
	{
		List<TodoTask> GetAllForToday();
		List<TodoTask> GetAllForTomorrow();
		List<TodoTask> GetAllForNext7Days();
		List<TodoTask> GetAllForYesterday();
		List<TodoTask> GetAllForPast();
		int GetCount();
	}

	public class TodoMemoryService : ITodoService
	{
		public List<TodoTask> GetAllForToday()
		{
			var result = TodoDb.GetAll().Where(u => u.StartTime.Date == DateTime.Now.Date).ToList();
			result.Sort();

			return result;
		}

		public List<TodoTask> GetAllForTomorrow()
		{
			var result = TodoDb.GetAll().Where(u => u.StartTime.Date == DateTime.Now.Date.AddDays(1)).ToList();
			result.Sort();

			return result;
		}

		public List<TodoTask> GetAllForNext7Days()
		{
			var result = TodoDb.GetAll().Where(u => (DateTime.Now.Date.AddDays(7) - u.StartTime.Date).TotalDays <= 7
			                                        && (DateTime.Now.Date.AddDays(7) - u.StartTime.Date).TotalDays >= 0)
				.ToList();
			result.Sort();

			return result;
		}

		public List<TodoTask> GetAllForYesterday()
		{
			var result = TodoDb.GetAll().Where(u => u.StartTime.Date == DateTime.Now.Date.AddDays(-1)).ToList();
			result.Sort();

			return result;
		}

		public List<TodoTask> GetAllForPast()
		{
			var result = TodoDb.GetAll().Where(u => u.StartTime.Date < DateTime.Now.Date).ToList();
			result.Sort();

			return result;
		}

		public int GetCount()
		{
			return TodoDb.GetAll().Count;
		}
	}
}