﻿using Android.App;
using Android.OS;
using Android.Widget;

namespace com.altexsoft.traineetasks.materialtheme
{
	[Activity(Label = "Task 9", MainLauncher = true, Icon = "@drawable/icon", Theme = "@style/Task8Theme")]
	public class MainActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			//var toolbar = FindViewById<Toolbar>(Resource.Id.);
			//SetActionBar(toolbar);
		}
	}
}
