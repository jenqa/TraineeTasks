﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace com.altexsoft.traineetasks.fragments
{
	public class FinalFragment : Fragment
	{
		public MainActivity OwnerActivity { get; private set; }

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			OwnerActivity = this.Activity as MainActivity;

			var view = inflater.Inflate(Resource.Layout.final_fragment, container, false);

			view.FindViewById<TextView>(Resource.Id.final_label_fn).Text = "First name: " + OwnerActivity.FirstName;
			view.FindViewById<TextView>(Resource.Id.final_label_ln).Text = "Last name: " + OwnerActivity.LastName;
			view.FindViewById<TextView>(Resource.Id.final_label_a).Text = "Age: " + OwnerActivity.Age;

			return view;
		}
	}
}