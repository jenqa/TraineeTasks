﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace com.altexsoft.traineetasks.fragments
{
	public abstract class BaseInputFragment : Fragment
	{
		internal MainActivity OwnerActivity { get; private set; }
		internal TextView LabelView { get; private set; }
		internal TextView ErrorView { get; private set; }
		internal EditText InputText { get; private set; }
		internal Button NextButton { get; private set; }

		public string TAG = typeof(BaseInputFragment).Name;

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			Log.Error(TAG, "OnCreateView()");

			OwnerActivity = Activity as MainActivity;

			var view = inflater.Inflate(Resource.Layout.input_fragment, container, false);

			LabelView = view.FindViewById<TextView>(Resource.Id.label);
			ErrorView = view.FindViewById<TextView>(Resource.Id.error);
			InputText = view.FindViewById<EditText>(Resource.Id.input);
			NextButton = view.FindViewById<Button>(Resource.Id.next);

			InputText.BeforeTextChanged += (s, e) => { if (ErrorView.Visibility == ViewStates.Visible) ErrorView.Visibility = ViewStates.Invisible; };

			NextButton.Click += OnNextClick;

			return view;
		}

		protected virtual void OnNextClick(object sender, EventArgs e)
		{
			OwnerActivity.MoveNext(this);
		}

		public override void OnAttach(Context context)
		{
			base.OnAttach(context);

			Log.Error(TAG, "OnAttach()");
		}

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			RetainInstance = true;

			Log.Error(TAG, "OnCreate()");
		}

		public override void OnActivityCreated(Bundle savedInstanceState)
		{
			base.OnActivityCreated(savedInstanceState);

			Log.Error(TAG, "OnActivityCreated()");
		}

		public override void OnStart()
		{
			base.OnStart();

			Log.Error(TAG, "OnStart()");
		}

		public override void OnResume()
		{
			base.OnResume();

			Log.Error(TAG, "OnResume()");
		}

		public override void OnPause()
		{
			base.OnPause();

			Log.Error(TAG, "OnPause()");
		}

		public override void OnStop()
		{
			base.OnStop();

			Log.Error(TAG, "OnStop()");
		}

		public override void OnDestroyView()
		{
			base.OnDestroyView();

			Log.Error(TAG, "OnDestroyView()");
		}

		public override void OnDestroy()
		{
			base.OnDestroy();

			Log.Error(TAG, "OnDestroy()");
		}

		public override void OnDetach()
		{
			base.OnDetach();

			Log.Error(TAG, "OnDetach()");
		}

		public override void OnSaveInstanceState(Bundle outState)
		{
			base.OnSaveInstanceState(outState);

			Log.Error(TAG, "OnSaveInstanceState()");
		}

	}
}