﻿using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Util;

namespace com.altexsoft.traineetasks.fragments
{
	[Activity(Label = "Task 8 (Fragments)", MainLauncher = true, Icon = "@drawable/icon",
		ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation)]
	public class MainActivity : Activity
	{
		public string TAG = typeof(MainActivity).Name;

		private List<Fragment> _chainOfFragments;

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Age { get; set; }

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			Log.Error(TAG, "OnCreate()");

			SetContentView(Resource.Layout.activity_main);

			_chainOfFragments = new List<Fragment>();
			_chainOfFragments.Add(new FirstNameInputFragment());
			_chainOfFragments.Add(new LastNameInputFragment());
			_chainOfFragments.Add(new AgeInputFragment());
			_chainOfFragments.Add(new FinalFragment());

			var fragTrans = FragmentManager.BeginTransaction();
			if (bundle != null)
			{
				FirstName = bundle.GetString("first_name");
				LastName = bundle.GetString("last_name");
				Age = bundle.GetString("age");

				//fragTrans.Replace(Resource.Id.frag_cont, _chainOfFragments[bundle.GetInt("current_fragment")]);
			} else
			{
				fragTrans.Replace(Resource.Id.frag_cont, _chainOfFragments[0]);
			}

			fragTrans.Commit();
		}

		internal void MoveNext(Fragment fragment)
		{
			if (_chainOfFragments.IndexOf(fragment) == _chainOfFragments.Count - 1)
				return;

			var fragTrans = FragmentManager.BeginTransaction();

			foreach (var frag in _chainOfFragments)
			{
				if(frag.GetType().Equals(fragment.GetType()))
				{
					fragTrans.Replace(Resource.Id.frag_cont, _chainOfFragments[_chainOfFragments.IndexOf(frag) + 1]);
					break;
				}
				
			}

			//var fragTrans = FragmentManager.BeginTransaction();
			//fragTrans.Replace(Resource.Id.frag_cont, _chainOfFragments[_chainOfFragments.IndexOf(fragment) + 1]);
			fragTrans.AddToBackStack(null);
			
			fragTrans.Commit();
		}

		protected override void OnSaveInstanceState(Bundle outState)
		{
			base.OnSaveInstanceState(outState);

			Log.Error(TAG, "OnSaveInstanceState()");

			outState.PutString("first_name", FirstName);
			outState.PutString("last_name", LastName);
			outState.PutString("age", Age);

			outState.PutInt("current_fragment", _chainOfFragments.IndexOf(FragmentManager.FindFragmentById(Resource.Id.frag_cont)));
		}

		public MainActivity()
		{
			Log.Error(TAG, "Constructor()");
		}

		protected override void OnStart()
		{
			base.OnStart();

			Log.Error(TAG, "OnStart()");
		}

		protected override void OnResume()
		{
			base.OnResume();

			Log.Error(TAG, "OnResume()");
		}

		protected override void OnPause()
		{
			base.OnPause();

			Log.Error(TAG, "OnPause()");
		}

		protected override void OnStop()
		{
			base.OnStop();

			Log.Error(TAG, "OnStop()");
		}

		protected override void OnRestart()
		{
			base.OnRestart();

			Log.Error(TAG, "OnRestart()");
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();

			Log.Error(TAG, "OnDestroy()");
		}
	}
}



