﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace com.altexsoft.traineetasks.fragments
{
	public class LastNameInputFragment : BaseInputFragment
	{
		public LastNameInputFragment()
		{
			TAG = typeof(LastNameInputFragment).Name;
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = base.OnCreateView(inflater, container, savedInstanceState);

			LabelView.Text = "Last name:";
			InputText.Text = OwnerActivity.LastName;

			return view;
		}

		protected override void OnNextClick(object sender, EventArgs e)
		{
			OwnerActivity.LastName = InputText.Text.Trim();

			if (string.IsNullOrEmpty(OwnerActivity.LastName))
				ErrorView.Visibility = ViewStates.Visible;
			else
				base.OnNextClick(sender, e);
		}

		public override void OnSaveInstanceState(Bundle outState)
		{
			base.OnSaveInstanceState(outState);

			if (!string.IsNullOrWhiteSpace(InputText?.Text))
				OwnerActivity.LastName = InputText.Text.Trim();
		}
	}
}