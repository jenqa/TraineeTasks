﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace Task_15_SharedPreference
{
	[Activity(Label = "Task_15_SharedPreference", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			SetContentView (Resource.Layout.Main);
		}
	}
}