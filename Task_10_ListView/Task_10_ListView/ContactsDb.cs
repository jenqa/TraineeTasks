﻿using System.Collections.Generic;
using System.Linq;

namespace Task_10_ListView
{
	public static class ContactsDb
	{
		public static List<Contact> Contacts { get; private set; }

		static ContactsDb()
		{
			Contacts = new List<Contact> {
				new Contact { Id = 1, FirstName = "Eugene", Patronymic = "Nicolaevich", LastName = "Timchenko", PhoneNumber = "(097)555-44-33", Address = "Mira 1, kv. 1"},
				new Contact { Id = 2, FirstName = "Kostya", Patronymic = "Nicolaevich", LastName = "Gerasimenko", PhoneNumber = "(097)555-44-33",  Address = "Mira 2, kv. 2" },
				new Contact { Id = 3, FirstName = "Denis", Patronymic = "Sergeevich", LastName = "Kohan", PhoneNumber = "(097)555-44-33",  Address = "Mira 3, kv. 3" },
				new Contact { Id = 4, FirstName = "Alexander", Patronymic = "Evgenievich", LastName = "Popel", PhoneNumber = "(097)555-44-33",  Address = "Mira 4, kv. 4" }
			};
		}

		public static Contact GetById(long id)
		{
			return Contacts.FirstOrDefault(c => c.Id == id);
		}

		public static bool DeleteById(long id)
		{
			var contact = Contacts.FirstOrDefault(c => c.Id == id);
			return Contacts.Remove(contact);
		}
	}
}