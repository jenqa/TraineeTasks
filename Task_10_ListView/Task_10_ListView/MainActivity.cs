﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using System.Collections.Generic;
using Android.Content;
using static Android.Widget.AdapterView;

namespace Task_10_ListView
{
	[Activity(Label = "Task_10_ListView", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		private ListView _listView;
		private ContactListAdapter _listAdapter;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.main);

			_listView = FindViewById<ListView>(Resource.Id.contacts_list);
			_listAdapter = new ContactListAdapter(this, ContactsDb.Contacts);
			_listView.Adapter = _listAdapter;
			_listView.ItemClick += OnListItemClick;

			RegisterForContextMenu(_listView);
		}

		public void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			ShowContact(e.Id);
		}

		public override void OnCreateContextMenu(IContextMenu menu, View v, IContextMenuContextMenuInfo menuInfo)
		{
			base.OnCreateContextMenu(menu, v, menuInfo);

			switch (v.Id)
			{
				case Resource.Id.contacts_list:
					menu.Add(0, (int)ContactCtxMenu.INFO, 1, "Information");
					menu.Add(0, (int)ContactCtxMenu.EDIT, 1, "Edit");
					menu.Add(0, (int)ContactCtxMenu.DELETE, 1, "Delete");
					break;
			}
		}

		public override bool OnContextItemSelected(IMenuItem item)
		{
			AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.MenuInfo;

			switch (item.ItemId)
			{
				case (int)ContactCtxMenu.INFO: ShowContact(info.Id); break;
				case (int)ContactCtxMenu.EDIT: break;
				case (int)ContactCtxMenu.DELETE: DeleteContact(info.Id); break;
			}

			return base.OnContextItemSelected(item);
		}

		private void DeleteContact(long contactId)
		{
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.SetMessage("Are you sure you want to delete the contact?");
			dialog.SetPositiveButton(Resource.String.ok_btn, (d, id) => {
				var result = ContactsDb.DeleteById(contactId);
				if (result)
				{
					Toast.MakeText(this, "The contact is deleted", ToastLength.Short).Show();
					_listAdapter.NotifyDataSetChanged();
				}
				else
				{
					Toast.MakeText(this, "No such contact", ToastLength.Short).Show();
				}
			});

			dialog.SetNegativeButton(Resource.String.cancel_btn, (d, id) => { });

			dialog.Show();
			
		}

		private void ShowContact(long contactId)
		{
			var info = new Intent(this, typeof(ContactInfoActivity));
			info.PutExtra("contact_id", contactId);
			StartActivity(info);
		}
	}

	public class ContactListAdapter : BaseAdapter<Contact>
	{
		List<Contact> _contacts;
		Activity _ctx;

		public ContactListAdapter(Activity ctx, List<Contact> persons)
		{
			_contacts = persons;
			_ctx = ctx;
		}

		public override Contact this[int position] => _contacts[position];

		public override int Count => _contacts.Count;

		public override long GetItemId(int position)
		{
			return _contacts[position].Id;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;
			if (view == null)
				view = _ctx.LayoutInflater.Inflate(Resource.Layout.list_view_layout, null);

			var avatar = view.FindViewById<ImageView>(Resource.Id.avatar);
			var nameField = view.FindViewById<TextView>(Resource.Id.name);
			var phoneField = view.FindViewById<TextView>(Resource.Id.phone_number);

			nameField.Text = _contacts[position].FirstName + " " + _contacts[position].LastName;
			phoneField.Text = _contacts[position].PhoneNumber;

			//avatar.SetImageBitmap(BitmapFactory.DecodeByteArray(null, 0, 1));
			return view;
		}
	}

	public class Contact
	{
		public long Id { get; set; }

		public string FirstName { get; set; }

		public string Patronymic { get; set; }

		public string LastName { get; set; }

		public string PhoneNumber { get; set; }

		public byte[] Avatar { get; set; }

		public string Address { get; set; }
	}
}

