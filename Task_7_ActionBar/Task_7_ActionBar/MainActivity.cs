﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;

namespace Task_7_ActionBar
{
	[Activity(Label = "Task_7_ActionBar", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);
		}

		protected override void OnStart()
		{
			base.OnStart();
		}

		protected override void OnResume()
		{
			base.OnResume();
		}

		public override bool OnCreateOptionsMenu(IMenu menu)
		{
			MenuInflater.Inflate(Resource.Menu.ActionBarMenu, menu);

			return base.OnCreateOptionsMenu(menu);
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			if (item.ItemId != Resource.Id.navigationMenuItem3)
			{
				Intent secondActivity = new Intent(this, typeof(SecondActivity));
				StartActivity(secondActivity);
			}

			return base.OnOptionsItemSelected(item);
		}
	}
}