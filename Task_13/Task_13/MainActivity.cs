﻿using Android.Widget;
using Android.OS;
using System;
using Android.Views;
using Android.Support.V7.App;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using Android.App;

namespace Task_13
{
	[Activity(Label = "Task_13", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : AppCompatActivity, IDialogMessageListener, ICanDrawShape
	{
		int selectedItem = 0;

		private TextView _dialogsInfo;
		private Button _dialogBtn1;
		private Button _dialogBtn2;
		private Button _dialogBtn3;
		private Button _dialogBtn4;
		private Button _dialogBtn5;

		public void DrawShape(string text, int cornerRadius, bool isSquare)
		{
			AlertDialog dialog;

			using(AlertDialog.Builder builder = new AlertDialog.Builder(this))
			{
				builder.SetView(new DrawableShapeView(this, text, cornerRadius, isSquare));
				builder.SetCancelable(true);
				dialog = builder.Create();
			}
				
			dialog.Show();
		}

		public void SendMessage(params string[] messages)
		{
			_dialogsInfo.Text = string.Empty;

			foreach (var m in messages)
			{
				_dialogsInfo.Text += m + System.Environment.NewLine;
			}
		}

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

			_dialogsInfo = FindViewById<TextView>(Resource.Id.dialogs_info);
			_dialogBtn1 = FindViewById<Button>(Resource.Id.dialog_1);
			_dialogBtn2 = FindViewById<Button>(Resource.Id.dialog_2);
			_dialogBtn3 = FindViewById<Button>(Resource.Id.dialog_3);
			_dialogBtn4 = FindViewById<Button>(Resource.Id.dialog_4);
			_dialogBtn5 = FindViewById<Button>(Resource.Id.dialog_5);

			_dialogBtn1.Click += OnClickDialogButton1;
			_dialogBtn2.Click += OnClickDialogButton2;
			_dialogBtn3.Click += OnClickDialogButton3;
			_dialogBtn4.Click += OnClickDialogButton4;
			_dialogBtn5.Click += OnClickDialogButton5;
		}

		private void OnClickDialogButton5(object sender, EventArgs e)
		{
			AlertDialog dialog;

			using(AlertDialog.Builder builder = new AlertDialog.Builder(this))
			{
				builder.SetTitle(Resource.String.dialog_5_title);

				var view = LayoutInflater.Inflate(Resource.Layout.ShapeDialog, null);
				builder.SetView(view);

				var shapeText = view.FindViewById<EditText>(Resource.Id.shape_text);
				var cornerRadius = view.FindViewById<SeekBar>(Resource.Id.radius_value);
				var isSquareFlag = view.FindViewById<CheckBox>(Resource.Id.is_square);
				var choosenRadius = view.FindViewById<TextView>(Resource.Id.show_choosen_radius);

				cornerRadius.ProgressChanged += (s, ev) =>
				{
					choosenRadius.Text = ev.SeekBar.Progress.ToString();
				};

				builder.SetPositiveButton(Resource.String.dialog_5_ok_btn_text,
					(s, de) =>
					{
						string message1 = "Dialog 5: Text - " + shapeText.Text;
						string message2 = "Dialog 5: Radius - " + cornerRadius.Progress;
						string message3 = "Dialog 5: Square - " + isSquareFlag.Checked;
						SendMessage(message1, message2, message3);

						DrawShape(shapeText.Text, cornerRadius.Progress, isSquareFlag.Checked);
					});

				dialog = builder.Create();
			}
			
			dialog.Show();
		}

		private void OnClickDialogButton4(object sender, EventArgs e)
		{
			AlertDialog dialog = null;

			using(AlertDialog.Builder builder = new AlertDialog.Builder(this))
			{
				builder.SetTitle(Resource.String.dialog_4_title);

				builder.SetSingleChoiceItems(Resource.Array.dialog_4_strings, selectedItem,
					(s, ev) =>
					{
						selectedItem = ev.Which;
						string message = "Dialog 4: Selected item is " + Resources.GetStringArray(Resource.Array.dialog_4_strings)[selectedItem];
						SendMessage(message);

						dialog.Dismiss();
					});
				
				dialog = builder.Create();
			}

			dialog.Show();
		}

		private void OnClickDialogButton3(object sender, EventArgs e)
		{
			AlertDialog dialog;

			using (AlertDialog.Builder builder = new AlertDialog.Builder(this))
			{
				builder.SetTitle(Resource.String.dialog_3_title);

				var view = LayoutInflater.Inflate(Resource.Layout.LoginDialog, null);
				builder.SetView(view);

				builder.SetPositiveButton(Resource.String.dialog_3_signin_btn_text,
					(s, de) =>
					{
						string message1 = "Dialog 3: Username - " + view.FindViewById<EditText>(Resource.Id.username_input).Text;
						string message2 = "Dialog 3: Password - " + view.FindViewById<EditText>(Resource.Id.password_input).Text;
						SendMessage(message1, message2);
					});

				builder.SetNegativeButton(Resource.String.dialog_3_cancel_btn_text,
					(s, de) =>
					{
						string message = "Dialog 3: Cancel button was pressed";
						SendMessage(message);
					});

				dialog = builder.Create();
			}

			dialog.Show();
		}

		private void OnClickDialogButton2(object sender, EventArgs e)
		{
			AlertDialog dialog;

			using (AlertDialog.Builder builder = new AlertDialog.Builder(this))
			{
				builder.SetTitle(Resource.String.dialog_2_title);
				builder.SetPositiveButton(Resource.String.dialog_2_ok_btn_text,
					(s, de) =>
					{
						string message = "Dialog 2: Ok button was pressed";
						SendMessage(message);
					});

				builder.SetNegativeButton(Resource.String.dialog_2_cancel_btn_text,
					(s, de) =>
					{
						string message = "Dialog 2: Cancel button was pressed";
						SendMessage(message);
					});

				dialog = builder.Create();
			}

			dialog.Show();
		}

		private void OnClickDialogButton1(object sender, EventArgs e)
		{
			AlertDialog dialog;

			using (AlertDialog.Builder builder = new AlertDialog.Builder(this))
			{
				builder.SetTitle(Resource.String.dialog_1_title);
				builder.SetPositiveButton(Resource.String.dialog_1_btn_text,
					(s, de) =>
					{
						string message = "Dialog 1: Ok button was pressed";
						SendMessage(message);
					});

				dialog = builder.Create();
			}
				
			dialog.Show();
		}
	}

	public interface IDialogMessageListener
	{
		void SendMessage(params string[] messages);
	}

	public interface ICanDrawShape
	{
		void DrawShape(string text, int cornerRadius, bool isSquare);
	}
}

