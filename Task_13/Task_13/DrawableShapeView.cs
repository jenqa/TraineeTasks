﻿using Android.Content;
using Android.Graphics;
using Android.Views;

namespace Task_13
{
	public class DrawableShapeView : View
	{
		private string _text;
		private int _radius;

		private float _shapeWidth;
		private float _shapeHeight = 200;

		private bool _isSquare;

		public DrawableShapeView(Context context, string text, int radius, bool isSquare) : base(context)
		{
			_text = text;
			_radius = radius * 5;
			_isSquare = isSquare;

			_shapeWidth = isSquare ? _shapeHeight : _shapeHeight * 2;
		}

		public override void Draw(Canvas canvas)
		{
			base.Draw(canvas);

			Paint rectPaint = new Paint();
			rectPaint.Color = new Color(Color.Green);
			rectPaint.SetStyle(Paint.Style.Fill);
			RectF rect = new RectF(0, 0, _shapeWidth, _shapeHeight);
			canvas.DrawRoundRect(rect, _radius, _radius, rectPaint);

			Paint textPaint = new Paint(PaintFlags.LinearText);
			textPaint.TextSize = 100;
			textPaint.Color = new Color(Color.Black);
			canvas.DrawText(_text, 50, 100, textPaint);
		}
	}
}

