﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Graphics;
using Android.Content;
using Android.Support.V7.App;

namespace Task_14
{
	[Activity(Label = "Task_14", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : AppCompatActivity
	{
		private Button _simpleTextBtn;
		private Button _bigTextBtn;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			SetContentView (Resource.Layout.Main);
			
			_simpleTextBtn = FindViewById<Button>(Resource.Id.simple_text_btn);
			_bigTextBtn = FindViewById<Button>(Resource.Id.big_text_btn);

			_simpleTextBtn.Click += ShowSimpleTextNotification;
			_bigTextBtn.Click += ShowBigTextNotification;
		}

		private void ShowSimpleTextNotification(object sender, EventArgs e)
		{
			Notification.Builder builder = new Notification.Builder(this);
			builder.SetSmallIcon(Resource.Drawable.SmallIcon)
					//.SetLargeIcon(BitmapFactory.DecodeResource(Resources, Resource.Drawable.BigIcon))
					.SetContentTitle("You should do some work today")
					.SetDefaults(NotificationDefaults.Sound)
					.SetContentText("Do English homework and Android task");

			var notification = builder.Build();
			
			NotificationManager notificationManager = GetSystemService(NotificationService) as NotificationManager;
			notificationManager.Notify(1, notification);
		}

		private void ShowBigTextNotification(object sender, EventArgs e)
		{
			NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();

			Intent intent = new Intent(this, typeof(MainActivity));
			PendingIntent pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);

			string bigText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dui dui, mollis ac interdum at, scelerisque sit amet neque. Duis blandit nulla non dolor accumsan, non aliquet nulla molestie. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec sollicitudin rutrum magna sed malesuada. Cras vitae tincidunt lorem. Nullam laoreet nec velit ultricies efficitur. Pellentesque vehicula enim suscipit rutrum tincidunt. Praesent ut sem et magna dignissim ornare. Vestibulum rutrum, mauris a semper viverra, mauris augue dapibus dui, sit amet ullamcorper felis augue eget nibh. Nulla iaculis consectetur lectus hendrerit bibendum. Aliquam laoreet ante nibh, id efficitur ex lacinia eu. Donec fringilla egestas faucibus. Curabitur placerat est non mi congue, id consectetur ante lacinia.";
			bigTextStyle.BigText(bigText);
			bigTextStyle.SetSummaryText("Click");

			NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
			builder.SetSmallIcon(Resource.Drawable.SmallIcon)
					.SetLargeIcon(BitmapFactory.DecodeResource(Resources, Resource.Drawable.BigIcon))
					.SetContentTitle("You should do some work today")
					.SetDefaults((int)NotificationDefaults.Sound)
					.SetStyle(bigTextStyle);
			Notification notification = builder.Build();

			NotificationManager notificationManager = GetSystemService(NotificationService) as NotificationManager;
			notificationManager.Notify(2, notification);
		}
	}
}